# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.
"""

from django.contrib import admin
from trapper.apps.storage.models import (
    Resource, Collection, CollectionMember
)
from trapper.apps.storage.forms import ResourceAdminForm


class CollectionAdmin(admin.ModelAdmin):
    list_display = ('name', )


class ResourceAdmin(admin.ModelAdmin):
    form = ResourceAdminForm
    list_display = (
        'name', 'owner', 'status', 'prefixed_name', 'deployment',
        'inherit_prefix', 'custom_prefix',
    )

    def deployment_id(self, item):
        if item.deployment:
            return item.deployment_id
        else:
            return u''


class CollectionMemberAdmin(admin.ModelAdmin):
    search_fields = (
        'collection__name',
    )
    list_display = (
        'collection', 'user', 'get_level_display', 'date_created'
    )
    list_filter = ('level', 'user')


admin.site.register(Resource, ResourceAdmin)
admin.site.register(Collection, CollectionAdmin)
admin.site.register(CollectionMember, CollectionMemberAdmin)
