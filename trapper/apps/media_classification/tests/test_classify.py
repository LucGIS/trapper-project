# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse

from trapper.apps.common.utils.test_tools import (
    ExtendedTestCase, ResearchProjectTestMixin, SequenceTestMixin,
    ClassificationProjectTestMixin, CollectionTestMixin,
    ClassificationTestMixin, ClassificatorTestMixin
)

from trapper.apps.media_classification.models import (
    Classification, UserClassification
)

from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels, ClassificationStatus, ClassifyMessages
)


class BaseClassifyTestCase(
    ExtendedTestCase, SequenceTestMixin, ResearchProjectTestMixin,
    ClassificationProjectTestMixin, CollectionTestMixin,
    ClassificationTestMixin, ClassificatorTestMixin
):
    def setUp(self):
        super(BaseClassifyTestCase, self).setUp()
        self.summon_alice()
        self.summon_ziutek()


class AnonymousClassifyTestCase(BaseClassifyTestCase):
    """Classify process logic for anonymous users"""

    def test_details(self):
        """Anonymous user has to login before seeing classify form"""
        url = reverse(
            'media_classification:classify',
            kwargs={'project_pk': 1, 'collection_pk': 1, 'resource_pk': 1}
        )
        self.assert_forbidden(url)

    def test_create(self):
        """
        Anonymous user has to login before getting access to create
        classification action
        """
        url = reverse(
            'media_classification:classify',
            kwargs={'project_pk': 1, 'collection_pk': 1, 'resource_pk': 1}
        )
        self.assert_forbidden(url, method='post', data={})

    def test_update(self):
        """
        Anonymous user has to login before getting access to update
        classification action
        """
        url = reverse(
            'media_classification:classify',
            kwargs={'project_pk': 1, 'collection_pk': 1, 'resource_pk': 1}
        )
        self.assert_forbidden(url, method='post', data={})

    def test_approve(self):
        """
        Anonymous user has to login before getting access to approve
        classification action
        """
        url = reverse(
            'media_classification:classify',
            kwargs={'project_pk': 1, 'collection_pk': 1, 'resource_pk': 1}
        )
        self.assert_forbidden(
            url, method='post', data={'approve_classification': True}
        )

    def test_create_multiple(self):
        """Anonymous user has to login before getting access to create
        multiple classifications action"""
        url = reverse(
            'media_classification:classify',
            kwargs={'project_pk': 1, 'collection_pk': 1, 'resource_pk': 1}
        )
        self.assert_forbidden(
            url, method='post', data={'classify_multiple': True}
        )

    def test_box_approve(self):
        """
        Anonymous user has to login before getting access to approve
        classification action in classification box
        """
        url = reverse(
            'media_classification:classify_approve',
            kwargs={'pk': 1}
        )
        self.assert_auth_required(url, method='post', data={})


class ClassifyFormPermissionsTestCase(BaseClassifyTestCase):
    """Classify access create/update form permission logic for authenticated
    users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyFormPermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.classification = self.create_classification(
            owner=owner, resource=resource,
            collection=classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.APPROVED
        )

        self.user_classification = self.create_user_classification(
            owner=owner, classification=self.classification
        )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': classification_collection.pk,
                'resource_pk': resource.pk
            }
        )

    def test_owner(self):
        """Owner can see classify form"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_admin(self):
        """User with ADMIN role can see classify form"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_expert(self):
        """User with EXPERT role can see classify form"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_collaborator(self):
        """User with COLLABORATOR role can see classify form"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_no_roles(self):
        """User with no roles cannot see classify form"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)


class ClassifyCreatePermissionsTestCase(BaseClassifyTestCase):
    """Classify create process logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyCreatePermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        """Prepare all necessary objects to perform test for different
        project permissions"""
        resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.classification = self.create_classification(
            owner=owner, resource=resource,
            collection=classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.APPROVED
        )

        self.user_classification = self.create_user_classification(
            owner=owner, classification=self.classification
        )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': classification_collection.pk,
                'resource_pk': resource.pk,
                'user_pk': owner.pk
            }
        )

    def test_owner(self):
        """Project owner can create classification"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method='post', data={})

    def test_role_admin(self):
        """User with ADMIN role can create classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method='post', data={})

    def test_role_expert(self):
        """User with EXPERT role can create classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method='post', data={})

    def test_role_collaborator(self):
        """User with COLLABORATOR role can create classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method='post', data={})

    def test_no_roles(self):
        """User with no roles cannot create classification"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url, method='post', data={})


class ClassifyUpdatePermissionsTestCase(BaseClassifyTestCase):
    """Classify update logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyUpdatePermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        """Prepare all necessary objects to perform test for different
        project permissions"""
        resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        predefined_attrs = {
            u'annotations': u'true',
            u'required_annotations': u'false',
            u'target_annotations': u'S',
        }
        static_attrs_order = u'annotations'

        classificator = self.create_classificator(
            owner=self.alice, predefined_attrs=predefined_attrs,
            static_attrs_order=static_attrs_order
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project,
            classificator=classificator
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.classification = self.create_classification(
            owner=owner, resource=resource,
            collection=classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.APPROVED
        )

        self.user_classification = self.create_user_classification(
            owner=owner, classification=self.classification
        )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': classification_collection.pk,
                'resource_pk': resource.pk,
                'user_pk': owner.pk
            }
        )

    def test_owner(self):
        """Owner can update classification"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method='post', data={})
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_admin(self):
        """User with ADMIN role can update other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method='post', data={})
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_expert(self):
        """User with EXPERT role cannot update other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method='post', data={})
        self.assert_has_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role cannot update other user classification
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method='post', data={})
        self.assert_has_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_no_roles(self):
        """User with no roles cannot update other user classification"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url, method='post', data={})


class ClassifyApprovePermissionsTestCase(BaseClassifyTestCase):
    """Classify approve process logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyApprovePermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        """Prepare all necessary objects to perform test for different
        project permissions"""
        resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        predefined_attrs = {
            u'annotations': u'true',
            u'required_annotations': u'false',
            u'target_annotations': u'S',
        }
        static_attrs_order = u'annotations'

        classificator = self.create_classificator(
            owner=self.alice, predefined_attrs=predefined_attrs,
            static_attrs_order=static_attrs_order
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project,
            classificator=classificator
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.classification = self.create_classification(
            owner=owner, resource=resource,
            collection=classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.APPROVED
        )

        self.user_classification = self.create_user_classification(
            owner=owner, classification=self.classification
        )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': classification_collection.pk,
                'resource_pk': resource.pk,
                'user_pk': owner.pk
            }
        )

    def test_owner(self):
        """Project owner can approve other user classification"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={'approve_classification': True}
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_admin(self):
        """User with ADMIN role can approve other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={'approve_classification': True}
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_expert(self):
        """User with EXPERT role cannot approve other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={'approve_classification': True}
        )
        self.assert_has_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role cannot approve other user classification
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={'approve_classification': True}
        )
        self.assert_has_message(
            response, ClassifyMessages.MSG_PERMS_REQUIRED
        )

    def test_no_roles(self):
        """User with no roles cannot see classification details"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(
            self.url, method='post', data={'approve_classification': True}
        )


class ClassifyCreateMultiplePermissionsTestCase(BaseClassifyTestCase):
    """Classify multiple process permission logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyCreateMultiplePermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        """Prepare all necessary objects to perform test for different
        project permissions"""
        self.resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[self.resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': classification_collection.pk,
                'resource_pk': self.resource.pk,
                'user_pk': owner.pk
            }
        )

    def test_owner(self):
        """Project owner can create multiple classifications"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post',
            data={
                'classify_multiple': True,
                'selected_resources': self.resource.pk
            }
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_CLASSIFY_MULTIPLE_FAILED
        )

    def test_role_admin(self):
        """User with ADMIN role can create multiple classifications"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post',
            data={
                'classify_multiple': True,
                'selected_resources': self.resource.pk
            }
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_CLASSIFY_MULTIPLE_FAILED
        )

    def test_role_expert(self):
        """User with EXPERT role can create multiple classifications"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post',
            data={
                'classify_multiple': True,
                'selected_resources': self.resource.pk
            }
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_CLASSIFY_MULTIPLE_FAILED
        )

    def test_role_collaborator(self):
        """User with COLLABORATOR role can create multiple classifications"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post',
            data={
                'classify_multiple': True,
                'selected_resources': self.resource.pk
            }
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_CLASSIFY_MULTIPLE_FAILED
        )

    def test_no_roles(self):
        """User with no roles cannot create classification"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(
            self.url, method='post',
            data={
                'classify_multiple': True,
                'selected_resources': self.resource.pk
            }
        )


class ClassifyBoxApprovePermissionsTestCase(BaseClassifyTestCase):
    """Classify process logic for anonymous users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyBoxApprovePermissionsTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self, owner, roles):
        """Prepare all necessary objects to perform test for different
        project permissions"""
        self.resource = self.create_resource(owner=self.alice)
        collection = self.create_collection(
            owner=owner, resources=[self.resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=collection
        )

        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project
        )

        classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.classification = self.create_classification(
            owner=owner, resource=self.resource,
            collection=classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.REJECTED
        )

        self.user_classification = self.create_user_classification(
            owner=owner, classification=self.classification
        )

        self.url = reverse(
            'media_classification:classify_approve',
            kwargs={
                'pk': self.user_classification.pk,
            }
        )

    def test_owner(self):
        """Project owner can approve own classifications"""
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={}, follow=True
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_APPROVE_PERMS
        )

    def test_role_admin(self):
        """User with ADMIN role can approve other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={}, follow=True
        )
        self.assert_has_no_message(
            response, ClassifyMessages.MSG_APPROVE_PERMS
        )

    def test_role_expert(self):
        """User with EXPERT role cannot approve other user classification"""
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={}, follow=True
        )
        self.assert_has_message(
            response, ClassifyMessages.MSG_APPROVE_PERMS
        )

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role cannot approve other user classification
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={}, follow=True
        )
        self.assert_has_message(
            response, ClassifyMessages.MSG_APPROVE_PERMS
        )

    def test_no_roles(self):
        """User with no roles cannot approve other user classification"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method='post', data={}, follow=True
        )
        self.assert_has_message(
            response, ClassifyMessages.MSG_APPROVE_PERMS
        )


class ClassifyTestCase(BaseClassifyTestCase):

    def setUp(self):
        """Login alice by default for all tests"""
        super(ClassifyTestCase, self).setUp()
        self.login_alice()

    def _call_helper(self):
        """Prepare all necessary objects to perform test for different
        project permissions"""

        owner = self.alice
        roles = None

        self.resource = self.create_resource(owner=self.alice)
        self.collection = self.create_collection(
            owner=owner, resources=[self.resource]
        )

        research_project = self.create_research_project(
            owner=owner
        )

        research_collection = self.create_research_project_collection(
            project=research_project,
            collection=self.collection
        )

        predefined_attrs = {
            u'comments': u'true',
            u'required_comments': u'false',
            u'target_comments': u'S',
        }
        static_attrs_order = u'comments'

        classificator = self.create_classificator(
            owner=self.alice, predefined_attrs=predefined_attrs,
            static_attrs_order=static_attrs_order
        )
        self.classification_project = self.create_classification_project(
            owner=owner, roles=roles, research_project=research_project,
            classificator=classificator
        )

        self.classification_collection = \
            self.create_classification_project_collection(
                project=self.classification_project,
                collection=research_collection
            )

        self.url = reverse(
            'media_classification:classify',
            kwargs={
                'project_pk': self.classification_project.pk,
                'collection_pk': self.classification_collection.pk,
                'resource_pk': self.resource.pk,
                'user_pk': owner.pk
            }
        )

    def test_create_without_classificator(self):
        """Without classificator assigned even if user has enough permissions
        new classification cannot be made"""
        self._call_helper()
        self.classification_project.classificator = None
        self.classification_project.save()

        response = self.assert_access_granted(self.url, method='post', data={})
        self.assert_has_message(
            response, ClassifyMessages.MSG_CLASSIFICATOR_MISSING
        )

    def test_create(self):
        """Using classify view user that has enough permissions can create
        `UserClassification` instance that is connected to `Classification`

        Newly created classification is not approved.
        """
        self._call_helper()

        params = {'comments': 'test comment'}
        self.assert_access_granted(
            self.url,
            method='post',
            data=params
        )

        self.assertTrue(
            Classification.objects.filter(
                resource=self.resource,
                collection=self.classification_collection,
                project=self.classification_project,
                approved_at__isnull=True,
            ).exists()
        )
        classification = Classification.objects.first()
        self.assertFalse(classification.static_attrs)

        self.assertTrue(
            UserClassification.objects.filter(
                classification=Classification.objects.first(),
                owner=self.alice
            )
        )
        user_classification = UserClassification.objects.first()
        self.assertEqual(
            user_classification.static_attrs['comments'], params['comments']
        )

    def test_update(self):
        """Using classify view user that has enough permissions can update
        `UserClassification` instance that is connected to `Classification`

        Updated classification is still not approved.
        """
        self._call_helper()

        classification = self.create_classification(
            owner=self.alice,
            resource=self.resource,
            collection=self.classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.REJECTED
        )
        self.create_user_classification(
            owner=self.alice,
            classification=classification,
        )

        params = {'comments': 'updated comment'}
        self.assert_access_granted(
            self.url,
            method='post',
            data=params
        )

        user_classification = UserClassification.objects.first()
        self.assertEqual(
            user_classification.static_attrs['comments'], params['comments']
        )

    def test_approve(self):
        """Using classify view user that has enough permissions can approve
        `Classification` with data from `UserClassification` instance
        """
        self._call_helper()

        classification = self.create_classification(
            owner=self.alice,
            resource=self.resource,
            collection=self.classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.REJECTED
        )
        self.create_user_classification(
            owner=self.alice,
            classification=classification,
        )

        params = {
            'comments': 'updated comment',
            'approve_classification': True
        }
        self.assert_access_granted(
            self.url,
            method='post',
            data=params
        )
        classification = Classification.objects.first()

        self.assertEqual(classification.status, ClassificationStatus.APPROVED)
        self.assertEqual(classification.approved_by, self.alice)
        self.assertEqual(
            classification.static_attrs['comments'], params['comments']
        )

    def test_create_multiple(self):
        """Using classify view user that has enough permissions can create
        multiple `UserClassification` instances that are connected to
        multiple `Classification` objects

        Created classifications are not approved.
        """

        self._call_helper()
        resource2 = self.create_resource(owner=self.alice)
        self.collection.resources.add(resource2)

        params = {
            'comments': 'test comment',
            'classify_multiple': True,
            'selected_resources': ",".join(
                [str(self.resource.pk), str(resource2.pk)]
            )
        }
        self.assert_access_granted(
            self.url,
            method='post',
            data=params
        )

        self.assertTrue(
            Classification.objects.filter(
                resource=self.resource,
                collection=self.classification_collection,
                project=self.classification_project,
                approved_at__isnull=True,
            ).exists()
        )

        classification = Classification.objects.get(
            resource=self.resource,
            collection=self.classification_collection,
            project=self.classification_project,
            approved_at__isnull=True,
        )
        self.assertFalse(classification.static_attrs)

        self.assertTrue(
            UserClassification.objects.filter(
                classification=classification,
                owner=self.alice
            )
        )
        user_classification = UserClassification.objects.get(
            owner=self.alice,
            classification=classification
        )
        self.assertEqual(
            user_classification.static_attrs['comments'], params['comments']
        )

        self.assertTrue(
            Classification.objects.filter(
                resource=resource2,
                collection=self.classification_collection,
                project=self.classification_project,
                approved_at__isnull=True,
            ).exists()
        )

        classification2 = Classification.objects.get(
            resource=resource2,
            collection=self.classification_collection,
            project=self.classification_project,
            approved_at__isnull=True,
        )
        self.assertFalse(classification2.static_attrs)

        self.assertTrue(
            UserClassification.objects.filter(
                classification=classification2,
                owner=self.alice
            )
        )
        user_classification2 = UserClassification.objects.get(
            owner=self.alice,
            classification=classification2
        )
        self.assertEqual(
            user_classification2.static_attrs['comments'], params['comments']
        )

    def test_box_approve(self):
        """Using classify view user that has enough permissions can approve
        other classifications using classification box
        """
        self._call_helper()

        classification = self.create_classification(
            owner=self.alice,
            resource=self.resource,
            collection=self.classification_collection,
            project=self.classification_project,
            status=ClassificationStatus.REJECTED
        )
        user_classification = self.create_user_classification(
            owner=self.alice,
            classification=classification,
        )

        url = reverse(
            'media_classification:classify_approve',
            kwargs={'pk': user_classification.pk}
        )

        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 302)
        classification = Classification.objects.get(pk=classification.pk)
        self.assertEqual(classification.status, ClassificationStatus.APPROVED)
        self.assertEqual(classification.approved_by, self.alice)
