# -*- coding: utf-8 -*-
"""
Helper functions to decode structured data strored by django-hstore fields
"""
import json
import datetime

import lxml.html.clean as clean

from django.utils import timezone


def json_loads_helper(obj):
    """Try convert given object into json"""
    try:
        return json.loads(obj)
    except (TypeError, ValueError):
        return obj


def parse_hstore_field(data):
    """Convert hstore value stored in database into python dictionary"""
    return dict(
        map(lambda (k, v): (k, json_loads_helper(v)), data.iteritems())
    )


def datetime_aware(data=None):
    """
    Make datetime object timezone aware.
    By default return timezone aware datatime.datetime.now()
    """
    if data is None:
        data = datetime.datetime.now()
    return timezone.make_aware(data, timezone.get_current_timezone())


def parse_pks(pks):
    """Method used to parse string with comma separated numbers"""
    output = []

    if isinstance(pks, (str, unicode)):
        for value in pks.split(','):
            try:
                value = int(value.strip())
            except ValueError:
                pass
            else:
                output.append(value)
    return output


def clean_html(value):
    """
    Clean html value and strip potentially dangerous code using
    :class:`lxml.html.clean.Cleaner`
    """
    cleaned = ''
    if value and value.strip():
        cleaner = clean.Cleaner(
            safe_attrs_only=True, safe_attrs=frozenset(['href'])
        )
        cleaned = cleaner.clean_html(value)
        # Cleaner wraps with p tag, it should be removed
        if cleaned.startswith('<p>') and cleaned.endswith('</p>'):
            cleaned = cleaned[3:-4]
    return cleaned
